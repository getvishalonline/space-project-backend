require 'sinatra'
require 'sinatra/namespace'
require 'sinatra/reloader'
require "sinatra/cors"
require_relative 'lib/sun'
require_relative 'models/pi'
require 'bigdecimal'


class App < Sinatra::Base
  register Sinatra::Namespace
  register Sinatra::Cors

  set :allow_origin, "*"
  set :allow_methods, "GET,HEAD,POST"
  set :allow_headers, "content-type,if-modified-since"
  set :expose_headers, "location,link"

  configure :development do
    register Sinatra::Reloader
  end

  configure do
    set :root, File.join(File.dirname(__FILE__))
    set :db_config, Proc.new { File.join(root, "config", "mongoid.yml") }
  end

  Mongoid.load! db_config

  # Routes

  namespace "/api/v1" do
    get "/get_info" do
      pi = Pi.last
      
      {pi: pi.try(:current_pi), circumference: pi.try(:circumference), radius: 696340 }.to_json
    end

    post "/sun_circumference" do
      last_pi = {}
      sun = Sun.new
      active_pi = Pi.last
      if active_pi
        current_pi = sun.get_pi(active_pi)
        new_pi = active_pi.current_pi + current_pi.pi
      else
        current_pi = sun.get_pi()
        new_pi = current_pi.pi
        active_pi = Pi.new
      end
  
      circumference = sun.circumference(BigDecimal(new_pi))
      active_pi.update_attributes({current_pi: new_pi, nn: current_pi.nn, nr: current_pi.nr, q: current_pi.q, 
      r: current_pi.r, t: current_pi.t, k: current_pi.k, n: current_pi.n, l: current_pi.l, circumference: circumference})

      {circumference: circumference, pi: active_pi.current_pi}.to_json
    end
  end
end

