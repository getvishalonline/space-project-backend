## Getting Started

Requirements:

- Ruby: 2.7.1
- Bundler
- Mongodb


Go to project directory then run:

```bash
  bundle install
```

Create `mongoid.yml` file inside config folder and then add the line below:

```yml
development:
  clients:
    default:
      database: space_dev
      hosts:
        - localhost:27017
```


### Run Application

```bash
  rackup config.ru
```

