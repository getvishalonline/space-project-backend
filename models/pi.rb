require 'mongoid'

class Pi
  include Mongoid::Document

  field :current_pi, default: ""
  field :nn, type: String
  field :nr, type: String
  field :q, type: String  
  field :r, type: String
  field :t, type: String 
  field :k, type: String
  field :n, type: String
  field :l, type: String
  field :circumference, type: String


  def as_json(opts = {})
    super(except: [:_id])
  end
end