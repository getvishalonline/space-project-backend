require_relative 'spigot'
require 'bigdecimal'

class Sun

  def initialize
    @radius = 696340 # As of 12/09/2021
  end

  def circumference(pi)
    BigDecimal("#{2 * pi * @radius}")
  end

  def get_pi(last_pi = nil)
    unless last_pi.nil?
      pi = Spigot.new(last_pi.nn.to_i, last_pi.nr.to_i, last_pi.q.to_i, last_pi.r.to_i, last_pi.t.to_i, last_pi.k.to_i, last_pi.n.to_i, last_pi.l.to_i, true)
      pi.calculate
    else
      pi = Spigot.new
      pi.calculate(2)
    end
  end
end