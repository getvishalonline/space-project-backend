class Spigot
  attr_accessor :q, :r, :t, :k, :n, :l, :nr, :nn, :pi, :started

  def initialize(nn = 0, nr = 0, q = 1, r = 0, t = 1, k = 1, n = 3, l = 3, started = false)
    self.pi = ""
    self.nn = nn
    self.nr = nr
    self.q = q
    self.r = r
    self.t = t
    self.k = k
    self.n = n
    self.l = l
    self.started = started
  end

  def update(q,r,t,k,n,l)
    self.q = q
    self.r = r
    self.t = t
    self.k = k
    self.n = n
    self.l = l
  end

  def calculate(limit=1)
    loop do
      if 4*q+r-t < n*t
        self.pi += n.to_s
        self.nr = 10*(r-n*t) # (r - (n * t)) * 10
        self.n = ((10*(3*q+r)) / t) - 10*n # (q * 3 + r) * 10 / t - (n * 10)
        self.q *= 10 # q * 10
        self.r = nr
        unless started
          self.started = !started
          self.pi += "."
        end

        limit -= 1
        break if limit == 0
      else
        self.nr = (2*q+r) * l
        self.nn = (q*(7*k+2)+r*l) / (t*l)
        self.q *= k
        self.t *= l
        self.l += 2
        self.k += 1
        self.n = nn
        self.r = nr
      end
    end
    self
  end
end